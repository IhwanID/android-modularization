package id.ihwan.androidmodularization

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.ihwan.testlib.MainActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_go_1.setOnClickListener {
            val i = Intent(this, MainActivity::class.java)
           startActivity(i)
        }
    }
}
